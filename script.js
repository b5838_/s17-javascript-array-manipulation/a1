// console.log("Hello World");

let students = [];

// 3. Create an addStudent() function that will accept a name of the student and add it to the student array.
function addStudent(student){
    students.push(student);
    console.log(student + " was added  to the student's list.")
}

// 4. Create a countStudents() function that will print the total number of students in the array.
function countStudents(){
    let count = students.length
    console.log("There are a total of " + count + " students enrolled.")
}

// Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.
function printStudents(){
    students.sort()
    students.forEach(function(student){
        console.log(student);
    })
}

/*
    6. Create a findStudent() function that will do the following:
    -Search for a student name when a keyword is given (filter method).
    -If one match is found print the message studentName is an enrollee.
    -If multiple matches are found print the message studentNames are enrollees.
    -If no match is found print the message studentName is not an enrollee.
    -The keyword given should not be case sensitive.
*/

function findStudent(name){

    let find = students.filter(function(student){
        return student.toLowerCase().includes(name);
    })
    if(find.length >= 1){
        console.log(find + " is an Enrollee");
    }
    else if(find.length >= 2){
        console.log(find + " are enrollees.")
    }
    else{
        console.log(name + " is not an enrollee");
    }
}